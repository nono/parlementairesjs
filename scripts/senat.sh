#!/usr/bin/env bash

# Extract data from the SQL files here: https://data.senat.fr/data/senateurs/export_sens.zip to data/senat/

set -e

cd $(dirname $0)

rm -rf "$(pwd)/data/senat/postgres" || true

rm ../data/senat/*.yml || true

initdb --auth=trust --username=postgres -D "$(pwd)/data/senat/postgres" > /dev/null

mkdir -p $(pwd)/data/senat/postgres-tmp || true

pg_ctl -D "$(pwd)/data/senat/postgres" -o "-c unix_socket_directories='$(pwd)/data/senat/postgres-tmp'" start > /dev/null

psql --host=127.0.0.1 --port=5432 --username=postgres --no-password postgres -c "CREATE USER opendata WITH SUPERUSER;" > /dev/null
psql --host=127.0.0.1 --port=5432 --username=postgres --no-password postgres -c "CREATE DATABASE opendata OWNER opendata;" > /dev/null

psql --host=127.0.0.1 --port=5432 --username=postgres --no-password opendata < "$(pwd)/data/senat/export_sens.sql" > /dev/null

psql --host=127.0.0.1 --port=5432 --username=postgres --no-password opendata -c "\t" -c "\a" -c "\o $(pwd)/data/senat/sen.json" -c "SELECT row_to_json(r) FROM sen AS r WHERE etasencod = 'ACTIF';" > /dev/null
psql --host=127.0.0.1 --port=5432 --username=postgres --no-password opendata -c "\t" -c "\a" -c "\o $(pwd)/data/senat/senurl.json" -c "SELECT row_to_json(r) FROM senurl AS r;" > /dev/null
psql --host=127.0.0.1 --port=5432 --username=postgres --no-password opendata -c "\t" -c "\a" -c "\o $(pwd)/data/senat/com.json" -c "SELECT row_to_json(r) FROM com AS r;" > /dev/null
psql --host=127.0.0.1 --port=5432 --username=postgres --no-password opendata -c "\t" -c "\a" -c "\o $(pwd)/data/senat/memcom.json" -c "SELECT row_to_json(r) FROM memcom AS r;" > /dev/null
psql --host=127.0.0.1 --port=5432 --username=postgres --no-password opendata -c "\t" -c "\a" -c "\o $(pwd)/data/senat/grppol.json" -c "SELECT row_to_json(r) FROM grppol AS r;" > /dev/null
psql --host=127.0.0.1 --port=5432 --username=postgres --no-password opendata -c "\t" -c "\a" -c "\o $(pwd)/data/senat/telephone.json" -c "SELECT row_to_json(r) FROM telephone AS r;" > /dev/null
psql --host=127.0.0.1 --port=5432 --username=postgres --no-password opendata -c "\t" -c "\a" -c "\o $(pwd)/data/senat/mel.json" -c "SELECT row_to_json(r) FROM mel AS r;" > /dev/null
psql --host=127.0.0.1 --port=5432 --username=postgres --no-password opendata -c "\t" -c "\a" -c "\o $(pwd)/data/senat/poicon.json" -c "SELECT row_to_json(r) FROM poicon AS r;" > /dev/null

pg_ctl -D "$(pwd)/data/senat/postgres" -o "-c unix_socket_directories='$(pwd)/data/senat/postgres-tmp'" stop > /dev/null

rm -rf "$(pwd)/data/senat/postgres" "$(pwd)/data/senat/postgres-tmp"

for row in $(cat ./data/senat/sen.json | jq -r '. | @base64'); do
  _jq() {
    echo ${row} | base64 --decode | jq -r ${1}
  }

  IFS=$'\n'
  id=$(_jq '.senmat')
  last_name=$(_jq '.sennomuse')
  first_name=$(_jq '.senprenomuse')
  groupRef=$(_jq '.sengrppolliccou')
  group=$(jq -r "select(.grppolliccou==\"${groupRef}\") | .grppollilcou" data/senat/grppol.json)
  county=$(_jq '.sencircou')

  poiconId=$(jq -r "select(.senmat==\"${id}\") | .poiconid" data/senat/poicon.json | sort -u)

  commissionsRef=$(jq -r "select(.senmat==\"${id}\" and .temvalcod==\"ACTIF\" and .memcomtitsup!=\"SUPPLEANT\") | .orgcod" data/senat/memcom.json | sort -u)
  commissions=""
  if [ ! -z "$commissionsRef" ]; then
    for i in $commissionsRef; do
      comNom=$(jq -r "select(.orgcod==\"${i}\" and (.typorgcod==\"COM\" or .typorgcod==\"COMEUR\") and .orgcod!=\"NEANT\") | .evelib" data/senat/com.json)
      if [ ! -z "$comNom" ]; then
        commissions="${commissions}"$'\n'"${comNom}"
      fi
    done
  fi

  email=$(_jq '.senema' | sed 's/null//i')
  phone=""
  
  if [ ! -z "$poiconId" ]; then
    for i in $poiconId; do
      phone="${phone}"$'\n'"$(jq -r "select(.poiconid==\"${i}\" and .telnum!=null) | .telnum" data/senat/telephone.json)"
      email="${email}"$'\n'"$(jq -r "select(.poiconid==\"${i}\" and .melema!=null) | .melema" data/senat/mel.json)"
    done
  fi

  twitter=$(jq -r "select((.senmat==\"${id}\") and (.typurlcod==\"TWEETER\") and (.temvalcod==\"ACTIF\")) | .senurlurl" data/senat/senurl.json | head -1 | sed 's/http:\/\/twitter\.com//i' | sed 's/https:\/\/twitter\.com//i' | sed 's/\/#!\///ig' | sed 's/\?lang=fr//i' | sed 's/https:\/tw\.unblock-us\.com\///i' | sed 's/\///ig')
  facebook=$(jq -r "select((.senmat==\"${id}\") and (.typurlcod==\"FACEBOOK\") and (.temvalcod==\"ACTIF\")) | .senurlurl" data/senat/senurl.json | head -1 | sed 's/https:\/\/fr-fr.facebook.com\///i' | sed 's/https:\/\/www.facebook.com\///i' | sed 's/http:\/\/fr-fr.facebook.com\///i' | sed 's/http:\/\/www.facebook.com\///i' | sed 's/home\.php?#!\///ig'  | sed 's/home\.php?#\///ig' | sed 's/?ref=profile//ig' | sed 's/?ref=ts//ig' | sed 's/&ref=profile//ig' | sed 's/&ref=ts//ig' | sed 's/?sk=wall//ig' | sed 's/&sk=wall//ig')
  photo=$(echo "${last_name}_${first_name}${id}" | iconv -f utf8 -t ascii//TRANSLIT | awk '{print tolower($0)}' | sed 's/-/_/ig' | sed 's/ /_/ig')

  if [ ! -f "../images/senat/${photo}.jpg" ]; then
    wget "http://www.senat.fr/senimg/${photo}.jpg" -O "../images/senat/${photo}.jpg"
  fi

  filename=$(echo "${last_name} ${first_name}" | iconv -f utf8 -t ascii//TRANSLIT | awk '{print tolower($0)}' | tr " " "_")

  echo "id: ${id}" > "../data/senat/${filename}.yml"
  echo "last_name: ${last_name}" >> "../data/senat/${filename}.yml"
  echo "first_name: ${first_name}" >> "../data/senat/${filename}.yml"
  echo "group: ${group}" >> "../data/senat/${filename}.yml"
  echo "county: ${county}" >> "../data/senat/${filename}.yml"

  echo "commissions:" >> "../data/senat/${filename}.yml"
  if [ ! -z "${commissions}" ]; then
    for i in ${commissions}; do
      echo "- \"${i}\"" >> "../data/senat/${filename}.yml"
    done
  fi

  echo "phone:" >> "../data/senat/${filename}.yml"
  if [ ! -z "${phone}" ]; then
    for i in ${phone}; do
      echo "- \"${i}\"" >> "../data/senat/${filename}.yml"
    done
  fi

  echo "email:" >> "../data/senat/${filename}.yml"
  if [ ! -z "${email}" ]; then
    for i in ${email}; do
      echo "- \"${i}\"" >> "../data/senat/${filename}.yml"
    done
  fi

  echo "twitter: ${twitter}" >> "../data/senat/${filename}.yml"
  echo "facebook: ${facebook}" >> "../data/senat/${filename}.yml"
  echo "photo: ${photo}" >> "../data/senat/${filename}.yml"
done

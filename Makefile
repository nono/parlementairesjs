all: an senat

an:
	mkdir -p json/
	yq -c -s . data/an/*.yml > json/an.json
	yq -c . data/an.yml > json/an-config.json

senat:
	mkdir -p json/
	yq -c -s . data/senat/*.yml > json/senat.json
	yq -c . data/senat.yml > json/senat-config.json
